import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'give-consent',
    pathMatch: 'full',
  },
  {
    path: 'give-consent',
    loadChildren: () =>
      import('./views/give-consent/give-consent.module').then(
        (m) => m.GiveConsentModule
      ),
  },
  {
    path: 'consents',
    loadChildren: () =>
      import('./views/consents-list/consents-list.module').then(
        (m) => m.ConsentsListModule
      ),
  },
  {
    path: '**',
    redirectTo: 'give-consent',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
