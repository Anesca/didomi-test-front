import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ConsentService } from 'src/app/core/services/consent.service';
import { Consent } from 'src/app/shared/models/consent.model';

@Component({
  selector: 'app-give-consent',
  templateUrl: './give-consent.component.html',
  styleUrls: ['./give-consent.component.sass'],
})
export class GiveConsentComponent implements OnInit {
  formGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    newsletter: new FormControl(false),
    targetAds: new FormControl(false),
    visits: new FormControl(false),
  });

  constructor(private consentService: ConsentService) {}

  sub!: Subscription;

  ngOnInit(): void {}

  checkValid() {
    //form is valid if name and email are ok and at least 1 consent is checked
    if (
      this.formGroup.get('username')?.valid &&
      this.formGroup.get('email')?.valid &&
      (this.formGroup.get('newsletter')?.value ||
        this.formGroup.get('targetAds')?.value ||
        this.formGroup.get('visits')?.value)
    )
      return true;
    else return false;
  }

  submitForm() {
    //check form valid
    if (!this.checkValid()) return;
    //Create consent data from form values
    const consent: Partial<Consent> = this.formGroup.value;
    //Call service to update DB
    this.sub = this.consentService.create(consent).subscribe((r) => ({
      next: () => {
        //Should notify the user at least
        console.log("success, don't forget to notify user :3");
        this.formGroup.reset();
      },
      error: (err: HttpErrorResponse) => {
        console.log("Error, don't forget to notify user :3");
        //Don't reset the forms if there was something wrong
      },
    }));
  }

  ngOnDestroy() {
    if (this.sub) this.sub.unsubscribe();
  }
}
