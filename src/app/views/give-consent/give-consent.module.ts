import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GiveConsentComponent } from './give-consent.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [GiveConsentComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: GiveConsentComponent }]),
    SharedModule,
    ReactiveFormsModule,
  ],
})
export class GiveConsentModule {}
