import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ConsentService } from 'src/app/core/services/consent.service';
import { Consent } from 'src/app/shared/models/consent.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-consents-list',
  templateUrl: './consents-list.component.html',
  styleUrls: ['./consents-list.component.sass'],
})
export class ConsentsListComponent implements AfterViewInit {
  //Create Material data source (needed for pagination)
  consentList: MatTableDataSource<Consent> = new MatTableDataSource();
  //Get paginator element
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  //column to display in the table
  columnsToDisplay = ['name', 'email', 'consent'];

  sub!: Subscription;

  constructor(private consentService: ConsentService) {}

  ngAfterViewInit(): void {
    //when paginator is ready we subscribe to the service to get all the consents
    this.sub = this.consentService.getAll().subscribe((consents) => {
      this.consentList = new MatTableDataSource(consents);
      this.consentList.paginator = this.paginator;
    });
  }

  ngOnDestroy() {
    if (this.sub) this.sub.unsubscribe();
  }
}
