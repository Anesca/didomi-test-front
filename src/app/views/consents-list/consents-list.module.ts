import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsentsListComponent } from './consents-list.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ConsentsListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: ConsentsListComponent }]),
    SharedModule,
  ],
})
export class ConsentsListModule {}
