import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, tap } from 'rxjs';
import { Consent } from 'src/app/shared/models/consent.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ConsentService {
  apiUrl = environment.apiUrl;

  private _consents = new BehaviorSubject<Consent[]>([]);
  public consents$ = this._consents.asObservable();

  constructor(private http: HttpClient) {}

  /**
   * Get all consent and update state
   * @returns observable of Consent[]
   */
  getAll() {
    return this.http
      .get<Consent[]>(this.apiUrl + '/consents')
      .pipe(tap((r) => this._consents.next(r)));
  }

  /**
   * get one consent and update state
   * @param id id of the consent needed
   * @returns observable of consent
   */
  getOne(id: number) {
    return this.http.get<Consent>(this.apiUrl + '/consents/' + id).pipe(
      tap((r) => {
        let consentList = this._consents.value;
        const i = this._consents.value.findIndex((c) => c.id === r.id);
        if (~i) consentList[i] = r;
        else consentList.push(r);
        return this._consents.next(consentList);
      })
    );
  }

  /**
   * create a new consent and update the state
   * @param consent consent's data
   * @returns an observable of newly created consent
   */
  create(consent: Partial<Consent>) {
    return this.http
      .post<Consent>(this.apiUrl + '/consents', consent)
      .pipe(tap((r) => this._consents.next([...this._consents.value, r])));
  }
}
