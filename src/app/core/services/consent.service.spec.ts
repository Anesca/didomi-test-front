/* tslint:disable */
import { TestBed, getTestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { switchMap, tap } from 'rxjs';
import { ConsentService } from './consent.service';
import { Consent } from 'src/app/shared/models/consent.model';
import { environment } from 'src/environments/environment';

fdescribe('UserResourceService', () => {
  //Define object to be used as mockup and dummy values
  let injector: TestBed;
  let service: ConsentService;
  let httpMock: HttpTestingController;
  const dummyConsents: Consent[] = [
    {
      id: 1,
      email: 'user.one@mail.com',
      username: 'user',
      newsletter: true,
      targetAds: true,
      visits: true,
    },
  ];
  const serviceUrl = environment.apiUrl + '/consents';

  function SettingFactory() {
    return environment;
  }

  beforeEach(() => {
    //Create a Testing module with the HttpClientTesting and the service to be tested
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ConsentService],
    });
    //Initialize the testBed and it's services
    injector = getTestBed();
    service = injector.inject(ConsentService);
    httpMock = injector.inject(HttpTestingController);
  });

  afterEach(() => {
    //After each test, verify that there isn't pending non-resolved request
    httpMock.verify();
  });

  //test the getAll function
  describe('#getAll', () => {
    it(`should call the right adress
        and return an Observable<Consent[]>
        and update the resources observable`, () => {
      //Call the function to be tested and subscribe to test the return value.
      service
        .getAll()
        .pipe(
          tap((resources) => {
            //We expect the response to match the dummy data that we'll pass to the request
            expect(resources).toEqual(dummyConsents);
          }),
          switchMap((resources) => service.consents$)
        )
        //We expect the service property to be affected by the request
        .subscribe((resources) => expect(resources).toEqual(dummyConsents));

      const req = httpMock.expectOne(serviceUrl);
      //check the request type
      expect(req.request.method).toBe('GET');
      //Gives the dummy data + Http infos as a response to our request
      req.flush(dummyConsents);
    });
  });

  //test the saveResource function
  describe('#saveResource', () => {
    it(`should call the right adress
        and return an Observable<Resource>
        and update the resources observable`, () => {
      //assign a previous value to be modified
      //@ts-ignore
      service._consents.next(dummyConsents);
      //create a new dummy to be the added
      const newConsent: Partial<Consent> = {
        username: 'hi',
        email: 'hi@coucou.hello',
        newsletter: true,
        targetAds: true,
        visits: true,
      };
      //Call the function to be tested and subscribe to test the return value.
      service.create(newConsent).subscribe(() => {
        let expectedDummy = [...dummyConsents, { ...newConsent, id: 2 }];

        //We expect the service property to be affected by the request
        //@ts-ignore
        expect(service.resources.value).toEqual(expectedDummy);
      });

      //Check that the request si made on the right adress
      const req = httpMock.expectOne(serviceUrl);

      //check the request type
      expect(req.request.method).toBe('POST');
      //Gives the dummy data + Http infos as a response to our request
      req.flush({ ...newConsent, id: 2 });
    });
  });
});
