import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ConsentService } from './services/consent.service';

@NgModule({
  declarations: [],
  providers: [ConsentService],
  imports: [CommonModule, HttpClientModule],
})
export class CoreModule {}
