export interface Consent {
  id: number;
  email: string;
  username: string;
  newsletter: boolean;
  targetAds: boolean;
  visits: boolean;
}
